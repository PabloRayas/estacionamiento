<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activo extends Model
{
  protected $fillable =
  [
    'id_cajon','codigo',
  ];
}
