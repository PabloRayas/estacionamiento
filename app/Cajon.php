<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cajon extends Model
{
    protected $table='cajones';
    public $timestamps=false;

    protected $fillable =
    [
    	'lat1','long1','lat2','long2','lat3','long3','lat4','long4',
    ];
}
