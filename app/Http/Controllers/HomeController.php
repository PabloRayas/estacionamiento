<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\userController;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userCont = new userController();

        $cantidad_user = $userCont->obtenerTotal();
        return view('home')->with('cantidad',$cantidad_user);
    }
}
