<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class adminController extends Controller
{
  public function mandarMensaje(Request $request)
  {
      $user = DB::table('users')->where('codigo','=', $request->codigo);

      //echo "usuario: " . $request->input("codigo");
      //echo "</br> mensaje: " . $request->input("mensaje");
      Notification::send($user, new InvoicePaid($request->mensaje));

      //return redirect('home');
  }

  public function vistaMensaje()
  {
      return view('nuevoMensaje');
  }
}
