<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Ixudra\Curl\Facades\Curl;
use App\Notifications\InvoicePaid;
use App\auto;
use Auth;
use App\user;
use Hash;
use DB;
use App\Activo;
use Notification;

class userController extends Controller
{
    public function ingresar()
    {
        return view('ingresar')->with('error',0);
    }

    public function autoCheck()
    {
        $auto = Activo::where('codigo', '=', Auth::user()->codigo)->first();

        if($auto==null)
        {
            return view('autoCheck');
        }
        else {
            return redirect('home');
        }
    }

    public function checkOutAuto()
    {
        DB::table('activos')->where('codigo', '=', Auth::user()->codigo)->delete();

        return redirect('home');
    }

    public function obtenerTotal()
    {
      $users = DB::table('activos')
      ->selectRaw('count(*) as user_count')
      ->first();

      $espacios = 260 - $users->user_count;

      return $espacios;
    }

    public function dejarAuto(Request $request)
    {
        $lugar = new Activo();
        $codigo_usuario = Auth::user()->codigo;
        $id_cajon = 2;

        if($request->ajax())
        {
            $latitude = $request->input("latitude");
            $longitud = $request->input("longitud");
            echo "latitude: " . $latitude;
            echo "longitud: " . $longitud;

            $this->ocuparCajon($id_cajon, $codigo_usuario);

            return redirect('home');
        }
    }

    public function mandarMensaje(Request $request)
    {
        $user = DB::table('users')->where('codigo','=', $request->codigo);

        //echo "usuario: " . $request->input("codigo");
        //echo "</br> mensaje: " . $request->input("mensaje");
        Notification::send($user, new InvoicePaid($request->mensaje));

        //return redirect('home');
    }

    public function vistaMensaje()
    {
        return view('nuevoMensaje');
    }

    public function ocuparCajon($id_cajon,$codigo_usuario)
    {
          $cajon = new Activo();

          $cajon->codigo = $codigo_usuario;
          $cajon->id_cajon = $id_cajon;

          $cajon->save();
    }

    public function viewAutos()
    {
        $auto = auto::where('codigo', '=', Auth::user()->codigo)->first();

        if($auto==null)
        {
            return view('autos');
        }
        else {
            return redirect('home');
        }
    }

    public function logear(Request $request)
    {
        //Auth::logout();
        $user = new User();
        $respuesta = Curl::to('http://148.202.152.33/ws_temporal.php')
        ->withData( array( 'codigo' => $request->codigo, 'nip' => $request->nip ) )
        ->post();

        $respuesta = $this->obtenerParametros($respuesta);

        if($respuesta[0] == 'A' || $respuesta[0] == 'T')
        {

            $usuario = User::where('codigo', '=', $request->codigo)->first();

            if($usuario==null)
            {
              $user->codigo = $request->codigo;
              $user->password = bcrypt($request->nip);
              $user->save();
            }

            if(Auth::attempt(['codigo' => $request->codigo, 'password' => $request->nip]))
            {
              $cantidad_user = $this->obtenerTotal();

              return view('home')->with('cantidad',$cantidad_user);
            }
        }
        else
        {
            return view('ingresar')->with('error',1);
        }
    }

    public function viewMap()
    {
        return view('maps');
    }

    public function obtenerParametros($respuesta)
    {
        $arregloAux = array();
        $parametros = array();
        $exclusion = array("{","}",'"');

        $arregloAux = str_replace($exclusion,"",$respuesta);
        $arregloAux = explode(":",$arregloAux);
        $parametros = explode(",",$arregloAux[1]);

        return $parametros;
    }

    public function registrarAuto(Request $request)
    {
        $auto = new auto();

        $auto->modelo = $request->input('modelo');
        $auto->tipo = $request->input('tipo');
        $auto->placas = $request->input('placas');
        $auto->codigo = Auth::user()->codigo;

        $auto->save();

        return redirect('home');
    }

    public function Salir()
    {
        Auth::logout();

        return redirect('/');
    }
}
