<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     public $timestamps = false;

    public function up()
    {
      Schema::create('autos', function (Blueprint $table) {
          $table->increments('id');
          $table->string('modelo');
          $table->string('tipo');
          $table->string('placas');
          $table->string('codigo');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autos');
    }
}
