<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCajonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cajones', function (Blueprint $table) {
            $table->increments('id');
            $table->double('lat1',15,8);
            $table->double('long1',15,8);
            $table->double('lat2',15,8);
            $table->double('long2',15,8);
            $table->double('lat3',15,8);
            $table->double('long3',15,8);
            $table->double('lat4',15,8);
            $table->double('long4',15,8);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cajones');
    }
}
