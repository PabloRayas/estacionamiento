@extends('layouts.app')

@section('content')


<button type="button" id="check" class="btn btn-info" >Dejar Automovil</button>


{!! Form::open(array('url'=>'dejarAuto','method'=>'POST', 'placeholder' => 'ID' ,'id' => 'form')) !!}
<meta name="_token" content="{!! csrf_token()!!}"/>
<input type="hidden" id="row_id" value="">
{!! Form::close() !!}

@endsection


@section('script')


<script>

$(document).ready(function()
{
    $(function()
    {
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content')}
        })
    });

    function getPosition(posicion)
    {
        var form = $('#form');
        var url = form.attr('action');
        var latitude = posicion.coords.latitude;
        var longitud = posicion.coords.longitude;

        $.ajax
        ({
                type:"POST",
                url: url,
                data: {latitude:latitude,longitud:longitud},
                dataType: 'html',
                success: function(result)
                {
                    
                }
          });
    }

    function error()
    {
        alert("error al obtener la posicion");
    }

    $('#check').click(function ()
    {

      navigator.geolocation.getCurrentPosition(getPosition, error);

    });
})

</script>
@endsection
