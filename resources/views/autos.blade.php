@extends('layouts.app')

@section('content')

<div class="panel panel-default" id="panel_autos">
<div class="panel-heading">Registrar Automovil</div>
<form method="post" action="{{'registrarAuto'}}">


    {!! csrf_field() !!}
    <div class="form-group" >
    <label for="modelo">Modelo</label>
    <input type="text" class="form-control" name="modelo" autocomplete="off">
    <label for="tipo">Tipo</label>
    <input type="text" class="form-control" name="tipo" autocomplete="off">
    <label for="placas">Placas</label>
    <input type="text" class="form-control" name="placas" autocomplete="off">
    <button type="submit" class="btn btn-default">Submit</button>
    </div>

</form>
</div>


@endsection
