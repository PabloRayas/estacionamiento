@extends('layouts.app')

@section('content')
      <div class="panel panel-default" id="panel_principal">
      <div class="panel-heading">INGRESAR</div>
      <form method="post" action="{{'logear'}}">

          {!! csrf_field() !!}
          
          <div class="form-group" >
          <label for="codigo">Codigo</label>
          <input type="text" class="form-control" name="codigo" autocomplete="off">
          <label for="password">Nip</label>
          <input type="password" class="form-control" name="nip" autocomplete="off">
          <button type="submit" class="btn btn-default">Submit</button>
          </div>

      </form>
      </div>

      @if($error == 1)
          <h2><label>Usuario incorrecto</label></h2>
      @endif

@endsection
