@extends('layouts.app')

@section('content')

    <title>Geolocalizacion</title>
	     <style>
		     #mapa{
			        margin: 20px;
		          }
	     </style>

      <button onclick="getMap()">Obtener coordenadas actuales</button>

      <div id="mapa"></div>

@endsection


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC82Ug4bvhPb7elFqi9bbL141v9owxBCCE"></script>
<script>
    function getMap()
    {
        var salida = document.getElementById('mapa');

        if(navigator.geolocation)
        {
            alert("si soporta geolocation");
        }
        else
        {
            alert("Tu navegador no soporta geolocalizacion");
        }

    function localizacion(posicion)
    {
        var latitude = posicion.coords.latitude;
        var longitud = posicion.coords.longitude;

        salida.innerHTML = "<p>Latitude: " + latitude + "<br> longitud: " + longitud + "<p>";

        var imgURL = "https://maps.googleapis.com/maps/api/staticmap?center="+latitude+","+longitud+
            "&size=600x300&markers=color:red%7C"+latitude+","+longitud+"&AIzaSyC82Ug4bvhPb7elFqi9bbL141v9owxBCCE";

        //salida.innerHTML ="<img src='"+imgURL+"'>";
    }

    function error()
    {
        alert("Algo ha ocurrido intenta de nuevo");
    }

    navigator.geolocation.getCurrentPosition(localizacion,error);
  }
</script>
