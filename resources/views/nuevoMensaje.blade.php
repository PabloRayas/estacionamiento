@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Nuevo Mensaje</div>

                <div class="panel-body">

                    <form method="post" action="{{'mandarMensaje'}}">

                        {!! csrf_field() !!}

                        <label for="codigo">Codigo del usuario</label>
                        <input type="text" class="form-control" name="codigo" autocomplete="off">
                        <label for="mensaje">Mensaje</label>
                        <input type="text" class="form-control" name="mensaje" autocomplete="off">
                        <button type="submit" class="btn btn-default">Submit</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
