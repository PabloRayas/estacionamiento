<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Estacionamiento Cucei</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body
            {
                background-color: #DCD6D6;
                color: BLACK;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            footer
            {
              border-color: black;
              border-width: 10px;
            }

            .full-height
            {
                height: 100vh;
            }

            .flex-center
            {
                align-items: flex-start;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 64px;
                padding-top: 40px;
            }

            .links > a {
                color: black;
                padding: 0 25px;
                font-size: 30px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 20px;
            }

            #panel_principal
            {
              margin: 100px;
              align-items: left;
              height: 200px;
              width: 700px;
              border-radius: 20px;
              border-color: black;
            }

        </style>
    </head>
    <body>

        {{ HTML::style('css/styles.css') }}

        <div class="flex-center position-ref full-height">


            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <!--a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a-->
                    @endif

                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Estacionamiento CUCEI
                </div>

                @if(Auth::guest())
                <div class="links">
                    <a href="{{'ingresar'}}">INGRESAR</a>
                </div>
                @endif

                {{ HTML::image('images/udg.png','alt', array( 'id' => 'udg' , 'align' => 'center')) }}

            </div>

          </div>

    </body>
    <footer>

    </footer>
</html>
