<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function ()
{
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function ()
{
    Route::get('autos', 'userController@viewAutos');

    Route::get('mapa', 'userController@viewMap');

    Route::post('registrarAuto', 'userController@registrarAuto');

    Route::get('autoCheck', 'userController@autoCheck');

    Route::post('dejarAuto', 'userController@dejarAuto');

    Route::post('mandarMensaje', 'userController@mandarMensaje');

    Route::get('checkOutAuto', 'userController@checkOutAuto');

    Route::get('vistaMensaje', 'adminController@vistaMensaje');

    Route::get('salir', 'userController@salir');
});

Route::get('ingresar', 'userController@ingresar');

Route::post('logear', 'userController@logear');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
